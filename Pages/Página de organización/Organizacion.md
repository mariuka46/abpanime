## Organización del proyecto 

![organizacion.jpg](organizacion.jpg)


### Gitlab y manejo de issues 

En este proyecto, se han realizado, creación de ramas, fusión de cambios y la resolución de conflictos que pueden surgir durante la integración de las ramas en la rama principal.

A lo largo del desarrollo del proyecto, se ha realizado un total de 99 commits y trabajado con 5 ramas diferentes.

### Milestones y reparto de tareas


Para la realización de este proyecto, se han establecido 2 milestones, cada uno correspondiente a un sprint específico. Cada milestone representa una etapa importante en el desarrollo del proyecto para mantenerse organizado.

El **primer milestone**, se ha dedicado al diseño y para el desarrollo del front-end. Durante este sprint, se dedicó el tiempo principalmente a diseñar las páginas esenciales de la aplicación.

El **segundo milestone** estuvo dedicado a la implementación del back-end de nuestra aplicación. Este sprint se enfocó en establecer la estructura básica del back-end, aunque se ha tenido que adaptar a lo largo del proyecto para satisfacer las necesidades cambiantes.

Estoy orgullosa del trabajo que se ha realizado, pese a que la aplicación no esté perfecta.

### Sprint Reviews y Sprint Plannings


Cada semana, se realizaba una reunión de **Sprint Planning**. Durante estas reuniones, se planeaban las tareas que debían realizarse en el próximo sprint.

Además, teníamos un tiempo establecido para realizar las **Sprints Reviews**. Estas reuniones se llevaban a cabo cada semana, o como máximo cada semana y media. Durante las Sprint Reviews, se revisaba y evaluaba lo que habíamos realizado durante el sprint.

El objetivo siempre ha sido llegar a los objetivos marcados, y se ha intentado hacer todo lo posible para lograrlo. 