# G7


# Documentación MarkDown App Anime

<img src="app/src/main/res/drawable/logo.jpg" alt="Logo aplicación" width="150"/>
---
Título: "El Rincon del Otaku"
<br>Autor: "Maria Cosa"
<br>Date: "23/04/2024"
---

## Presentación de los componentes del grupo

Entusiasta aprendiz de informática, se compromete a llevar a cabo adelante el proyecto así como dedicar tiempo a poder aprender lo que le hace falta para sacar adelante todos los contratiempos que se puedan presentar a lo largo del desarrollo, sacrificando noches de sueño para sacar adelante la primera entrega del proyecto


## Presentación del proyecto

El proyecto se trata de implementar tanto el backend como el frontend de una app de Anime que ofrece la posibilidad al usuario de visualizar los animes
de esta temporada, pudiendo ver la información en sí de estos y sus diferentes capitulates, asi como la posibilidad de agregarlos a la pantalla de favoritos
para tener los animes que más les gusten al alcance. El usuario puede ver sus datos en su perfil como también cambiarlos o eliminando su cuenta.

---
